/**
 * @defgroup   POST Power on self test 
 *
 * @brief      This file implements the Power on self-test for the 
 *              Patient monitoring device.
 *
 * @date       2020-04-09
 */

/**
 * TODO: Right now the range values cannot be decided on for any
 * of the sensor because there is no practical way to check it 
 * amid COVID lockdown due to lack of h/w. 
 *
 * Need to check and set these ranges for the POST to work
 */

#include <cstdio>
#include <stdarg.h>
//#include <shunyaInterfaces.h>
#include <functions.h>
#include <aws.h>
#include <pcf8591.h>
#include <curl/curl.h>
#include <string.h>
#include <sys/statvfs.h>
#include <time.h>

//#include "Nextion.h"
#include "MAX30100.h"
#include "whatsapp_alert.h"
#include "alarms.h"
#include "ecg.h"

#define LOG_FILE "/var/log/pmd_post.log"
#define CONF_FILE "/etc/pmd/pmd.conf"
#define TOPIC "pmd/post"
#define PAYLOAD "POST TEST"


MAX30100 *pulseOxymeter;

/* Callback functions for button click */
void alarmSilenceCallback(void *ptr);

int pressed = 0;

/* 
 * Global AWS settings 
 */
struct AWSSettings set = {
        "a3rh361ag55y5n-ats.iot.us-east-2.amazonaws.com",/* MQTT HOST URL */
        8883,/* MQTT port */
        "./modules/cloud/certs", /* Cert location */
        "AmazonRootCA1.pem", /* Client Root CA file name */
        "187a480729-certificate.pem.crt", /* Client certificate file name */
        "187a480729-private.pem.key", /* Client Private Key file name */
        1, /* Quality Of Service 0 or 1 */
        "test-3-25" /* Client ID */
};

int8_t aws_ok = 0;

/**
 * @brief      Writes a to the configuration.
 *
 * @param[in]  msg   The config 
 */
void write_config (int msg) 
{
	/* Declare a file pointer variable */
	FILE *fptr;

	/* Open the error database file */
	if ((fptr=fopen(CONF_FILE, "w")) == NULL) {
		/* Print error if unable to open the file*/
		fprintf(stderr, "Failed to open file %s", CONF_FILE);

	}
	/* Print time and message passed */
	fprintf(fptr, "%d\n", msg);

	/* Close the file */
	fclose(fptr);
}

/**
 * @brief      Writes to log.
 *
 * @param      msg   The message
 */
void write_to_log (char *msg, ...) 
{
        va_list ap;
        char payload[1024];

        va_start(ap, msg);
        vsnprintf(payload, sizeof(payload), msg, ap);
        va_end(ap);

        /* Declare a file pointer variable */
        FILE *fptr;

        /* Open the error database file */
        if ((fptr=fopen(LOG_FILE, "a")) == NULL) {
                /* Print error if unable to open the file*/
                fprintf(stderr, "Failed to open file %s", LOG_FILE);

        }

        /* Declare a variables for storing time info */
        time_t rawtime;
        struct tm * timeinfo;

        /* Get time from the system*/
        time(&rawtime);
        /* Convert this time into local time */
        timeinfo = localtime(&rawtime);

        /* Print time and message passed */
        fprintf(fptr, "%s%s\n", asctime(timeinfo), msg);

        /* Close the file */
        fclose(fptr);
}

/**
 * @brief Check the AD8232 ecg sensor
 */
int8_t check_ecg_sensor()
{
        int count = 0;
	for(int i =0; i<5; i++) {
		/* Read from the ecg sensor */
		/* Check if the value comes in range */

                if(egc_read()) {
                        write_to_log("ECG OK!");
                        count++;    
                } else {
                        /* Handle errors */
                        write_to_log("ECG test Failed: !");                
                }
	}
        if (count > 2) {
                return 0;    
        }

        return -1;
}

/**
 * @brief Check the MAX30100 heart rate and SpO2 sensor 
 *
 * @return 
 */
int8_t check_max30100 ()
{
        int count = 0;
        pulseOxymeter = new MAX30100();

	for(int i = 0; i < 5; i++) {
		/* Read from the sensor */
                pulseoxymeter_t result = pulseOxymeter->update();

                if (result.pulseDetected == true)
                {
                        /* Check if the value comes in range */
                        if(result.heartBPM && result.SaO2){
                                write_to_log("MAX30100 OK!");
                                count++;
                        }else {
                                /* Handle errors */
                                write_to_log("MAX30100 test Failed: !");
                        }
                }
	}
        if (count > 2) {
                return 0;    
        }

        return -1;
}

/**
 * @brief Calls the alarm silence function when button is pressed.
 *
 */
void alarmSilenceCallback(void *ptr)
{
        pressed++;
}

/**
 * @brief Check the MAX30100 heart rate and SpO2 sensor 
 *
 * @return 
 */
int8_t check_display ()
{
#if 0
        /**
         * Declare buttons here 
         */
        NexButton p0b0 = NexButton(0, 1, "page0.b0");

        /*
         * Register object n0, b0, b1, to the touch event list.  
         */
        NexTouch *nex_listen_list[] = 
        {
                &p0b0,
                NULL
        };

       /* Set the baud rate which is for debug and communicate with Nextion screen. */
        nexInit();

        /* Attach the silence callback function to the Silence button */
        p0b0.attachPop(alarmSilenceCallback, &p0b0);

        long lastTime = micros();
        /* Wait for user to press the alarm silence button for 20 secs*/
        while (micros() - lastTime > 20000 )
        {
                /*
                 * When a pop or push event occurred every time, 
                 * the corresponding component[right page id and component id] in touch 
                 * event list will be asked.
                 */
                nexLoop(nex_listen_list);   
        }

        if (pressed) {
                write_to_log("Display OK!");
                return 0;
        } else {
                write_to_log("Display test Failed: !");
        }
#endif
        return -1;

}
/**
 * @brief Check the MAX30100 heart rate and SpO2 sensor 
 *
 * @return 
 */
int8_t check_alarms ()
{
        /* Ring ALL alarms check if user pushes the alarm silence button */
        alarms_ring();
        write_to_log("Alarms OK!");
        //write_to_log("Alarms test Failed: !");

}

/**
 * @brief Check the MAX30100 heart rate and SpO2 sensor 
 *
 * @return 
 */
int8_t critical_functions_ok ()
{
	int8_t test_status = 0;
	/* Check the ECG sensor */
	test_status += check_ecg_sensor();

	/* Check the Heart rate and SpO2 sensor */
	test_status += check_max30100();

	/* Check the Display */ 
	test_status += check_display();

	/* Check Alarms */
	test_status += check_alarms();

	return test_status;
}
/* This part of the code is not mine and is copied from Stack over flow
 * https://stackoverflow.com/questions/1636333/download-file-using-libcurl-in-c-c
 */
size_t write_data(void *ptr, size_t size, size_t nmemb, FILE *stream) 
{
	size_t written = fwrite(ptr, size, nmemb, stream);
	return written;

}

/**
 * @brief Check the network connectivity  
 *
 * @return 0 if Internet connectivity is established, -1 if no Internet 
 */
int8_t check_network (char *url)
{

	CURL *curl;
	FILE *fp;

	CURLcode res;

	int8_t no_conn = 0;
	
	for(int i =0; i<5; i++) {

		curl = curl_easy_init();

		/* Check if the Internet is connected */
		curl_easy_setopt(curl, CURLOPT_URL, url);
		curl_easy_setopt(curl, CURLOPT_NOBODY, 1);
		res = curl_easy_perform(curl);
		if (res != CURLE_OK) {
			switch (res) {
				case CURLE_COULDNT_CONNECT:
				case CURLE_COULDNT_RESOLVE_HOST:
				case CURLE_COULDNT_RESOLVE_PROXY:
				 	no_conn++;
					break;
				default:
					printf("CUrl failed!");
			}
			curl_easy_cleanup(curl);
		}
	}
	if (no_conn > 2) {
                write_to_log("Network connectivity Failed: Failed to ping URL: %s!", url);
		return -1;
	}
        write_to_log("Network connected to URL: %s", url);
	return 0;
}

/**
 * @brief      Function takes action when the Remote sends control commands
 * 
 * This is where the commands are hard coded in.
 * 
 * TODO: Add more commands to the thing.
 *
 * @param[in]  topicNameLen  The topic name length
 * @param      topicName     The topic name
 * @param[in]  msgLen        The message length
 * @param      msg           The message
 */
void sub_checker(int topicNameLen, char *topicName, int msgLen, char *msg) 
{
        if (strncmp(topicName, TOPIC, strlen(TOPIC)) == 0 ) {
                if(strncmp(msg, PAYLOAD, strlen(PAYLOAD)) == 0 ) {
                        aws_ok++;
                }                
        }
}

/**
 * @brief      Connect to AWS MQTT and check connection
 *
 * @return     -1 if no connection and 0 on successful connection
 */
int8_t check_aws_connect () 
{
        /* Send to the AWS cloud */
        aws_iot_sub (set, TOPIC, strlen(TOPIC), sub_checker);
        for(int i =0; i<5; i++) {
                aws_iot_pub (set, TOPIC, strlen(TOPIC), PAYLOAD, strlen(PAYLOAD));
        }
        if (aws_ok > 2) {
                write_to_log("AWS MQTT test Passed!");
                return 0;
        }
        write_to_log("AWS MQTT test Failed!: No response from the MQTT broker.");
        return -1;
}

/**
 * @brief      Check for disk space 
 *
 * @return     -1 for no space and 0 for disk space. 
 */
int8_t check_disk_space() 
{
        struct statvfs stat;

        if (statvfs("/", &stat) != 0) {
                // error happens, just quits here
                return -1;
        }

        // the available size is f_bsize * f_bavail
        long disk_size = stat.f_bsize * stat.f_bavail;

        disk_size = (long)(disk_size / 1024) / 1024;

        if (disk_size > 100) {
                write_to_log("Sufficient Disk Space available!");
                return 0;
        }
        write_to_log("Insufficient Disk Space!: Space remaining %ld MB", disk_size);
        return -1;
}       

/**
 * @brief     Checks all non-critical features of the device 
 *
 * @return     return code for the check, -ve is error & 0 is SUCCESS
 */
int8_t check_other_features()
{
        int8_t rc = 0;
        /* Check Network connectivity */
        rc += check_network("www.google.com");
        if (rc == 0){
                /* Check AWS for Connectivity*/
                rc += check_aws_connect();
                /* Check NTP server */
                rc += check_network("0.in.pool.ntp.org");
        }
        /* Check Device space  */    
        rc += check_disk_space(); 
        return rc;        
}

int main(void)
{
        int8_t ret = -1;
        
        /* Shunya Interfaces Init*/
        libInit();

        /* Test critical features */
        ret = critical_functions_ok();

        if (ret < 0) {
                printf("Critical Functions Faulty \n");
                write_config(0);
                exit(1);
        } else {
                ret = -1;
                /* Test Other features  */
                ret = check_other_features();
                if (ret < 0) {
                        printf("Critical Functions ok, other features are disabled.\n");
                        write_config(1);
                } else {
                        printf("All ok\n");
                        write_config(2);     
                }                
        }
}
