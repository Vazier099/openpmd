#include <stdio.h>
#include <shunyaInterfaces.h>
#include <stdbool.h>

#include "MAX30100.h"
#include "ad8232.h"
#include "pcf8591.h"


struct alarmThershold {
        int8_t heart_rate_max;
        int8_t heart_reate_min;
        float percent_spo2_min;
};

static struct alarmThreshold thresh = {
        .heart_rate_max = DEFAULT_MAX_HR,
        .heart_reate_min = DEFAULT_MIN_HR,
        .percent_spo2_min = DEFAULT_MIN_SPO2
};

//Function for Manual settings and storing the conditions in txt file
void manual_settings()
{	
	FILE *fptr;

	int HR_lowerLimit;
	int HR_higherLimit;
	float spo2_val;
	
	fptr = fopen(STORAGE_FILE,"w");

	printf("Please enter the lower limit for heart rate(bpm): ");
	scanf("%d",&HR_lowerLimit);
	fprintf(fptr,"%d\n",HR_lowerLimit);

	printf("Please enter the upper limit for heart rate(bpm): ");
	scanf("%d",&HR_higherLimit);
	fprintf(fptr,"%d",HR_higherLimit);

	printf("Please enter the percentage limit for SpO2: ");
	scanf("%f",&spo2_val);
	fprintf(fptr,"%f",spo2_val);
	
	fclose(fptr);
}

//Function to start the alarm
void alarmLoop()
{	
        if (isECGAlarmOn){
                digitalWrite(LED_ECG, HIGH);
                if (isSoundAlarmOn) {
                        digitalWrite(buzzerPin, HIGH);                        
                }

        } else {
                digitalWrite(LED_ECG, LOW);
                digitalWrite(buzzerPin, LOW);
        }
        if (isSpO2AlarmOn){
                digitalWrite(LED_SPO2, HIGH);
                if (isSoundAlarmOn) {
                        digitalWrite(buzzerPin, HIGH);                        
                }

        } else {
                digitalWrite(LED_SPO2, LOW);
                digitalWrite(buzzerPin, LOW);
        }
        if (isHRAlarmOn){
                digitalWrite(LED_HEARTRATE, HIGH);
                if (isSoundAlarmOn) {
                        digitalWrite(buzzerPin, HIGH);                        
                }

        } else {
                digitalWrite(LED_HEARTRATE, LOW);
                digitalWrite(buzzerPin, LOW);
        }
}

int8_t writeDefaultAlarmstoFile()
{
        FILE *fptr;

        int8_t default_Higher_HR = thresh.heart_reate_max;
        int8_t default_Lower_HR = thresh.heart_reate_min;
        float default_limit_SpO2 = thresh.percent_spo2_min;

        fptr = fopen(STORAGE_FILE,"w");

        fprintf(fptr,"%d\n%d\n%f\n",default_Higher_HR,default_Lower_HR,default_limit_SpO2);

        fclose(fptr);
        // Add error handling here.
        return 0;
}

struct alarmsThreshold getAlarmsfromFile() 
{
        int ans;
        int8_t High_HR;
        int8_t Low_HR;
        float percent_spo2;

        //Setting default alarm conditions and storing them in txt file       
        //Asking for manual or default settings

        printf("Would you like to set the conditions manually or proceed with the default conditions?\nPress 1 for manual and 2 for default settings\n");

        scanf("%d",&ans);
        
        if(ans == 1){
                manual_settings();
                printf("Changed limits successfully\n");        
        }
        else{
                printf("Proceeding with default settings\n");
        }
        
        //Getting the limits from the txt file.
        FILE *fptr;

        fptr = fopen(STORAGE_FILE, "r");
        
        fscanf(fptr,"%d %d %f", &High_HR,&Low_HR,&percent_spo2);
        
        fclose(fptr);
}