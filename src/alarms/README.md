## Alarms in Patient Monitoring System  

* The parameters for which the alarm will be generated are Heart Rate, ECG(Asystole) and SpO2.  
* Each alarm has a dedicated led which will blink and a buzzer which will ring after the alarm condition is satisfied.  
* The default alarm conditions are stored in a text file using the functions:  
>writeDefaultAlarmstoFile();
* The doctor can change the alarm conditions and these conditions are stored in the same text file.The function used is  
> manual_settings()  
* The alarm conditions are retrieved from this text file as shown,  
> getAlarmsfromFile();  


The Alarms are as follows:  

**I. Alarm for ECG**  

1. The trigger for this alarm is the absence of QRS complex in the ECG graph.  
2. The algorithm used to detect the absence of QRS complex is the Peak threshold algorithm.  
3. A data point x(n) will be considered as QRS complex peak if the subtraction result of the current data point x (n), and previous data point x (n-1); then current data point  x(n) and its next data point x (n+1), is positive.
Besides that, the current data point’s value is more than threshold value.  

![PTM](/uploads/a1099453494a2ff63d2cac832cf5ca50/PTM.png)  

4. The sensor used to obtain the ECG is AD8232.  
5. ADC pcf8591 is used to obtain the value of the data point x(n+1).  
6. The function used to create the alarm for ECG is `void alarm_for_ECG()`.  
   * Check if the electrodes are connected.  	
   * Here lastECGval,secondLastECGval,currECGval represent x(n),x(n-1),x(n+1) respectively.  
   * Threshold = 0.00035  
   * The value currECGval is obtained using getAdc(), and then in an infinite loop the condition, as shown in the above fig is checked.  
   * If the above condition is satisfied, it means QRS complex is present else an alarm is triggered using a flag.  

**II. Alarm for No. of Heart Beats/min**  

1. For this alarm when the no. of heart beats/min do not satisfy a set criteria an alarm is triggered.  
2. The sensor used to detect the no. of heart beats/min is MAX30100.  
3. The function used is `void alarm_for_HeartRate(int HHR,int LHR)`.  
   * The number of heart beats/min obtained from the sensor are compared with the default conditions(HL:80bom & LL:60bpm) or as set by the doctor.  
   * If the bpm are less than the lower limit or higher than than the upper limit an alarm is triggered using triggered using a flag.  

**III. Alarm for SpO2**  

1. This alarm is used to indicate the oxygen saturation in the blood.  
2. The sensor used is MAX30100.  
3. The function used is `void alarm_for_SpO2(float val)`.
   * The percentage oxygen saturation obtained from the sensor is compared with the default SpO2 percentage(90) or as set by the doctor.  
   * If its value is less than the set value an alarm is initiated using a flag.  

**Function alarm_loop()**  

1. This function when called blinks the LED and rings the buzzer, based on the flags.  
