## Steps to compile and run example code in a ShunyOS Docker Container



First, install dependencies for running the shunyaos container, to make sure it works on our x86_64 systems:

```bash
sudo apt-get install qemu binfmt-support qemu-user-static
```

Make sure you run the following command before starting the shunyaos container, or you'll get errors in doing so.

```bash
docker run --rm --privileged multiarch/qemu-user-static --reset -p yes 
```



Pull the shunyaos/shunya:armv8 image.

```bash
docker pull shunyaos/shunya:armv8
```

Then, make sure your code is at a folder, say `/home/../<your-folder>/max30100` for this example.
Then, open a shell prompt in the docker container by bindmounting the before-mentioned directory to a directory on docker, say `/src/max30100`.

```bash
docker run -v /home/../<your-folder>/max30100:/src/max30100 -ti shunyaos/shunya:armv8 /bin/bash
```



Now, you can edit the files in your host system with your favourite text editor and compile them in the ShunyaOS container as follows:

```bash
cd /src/max30100
g++ MAX30100_example.cpp MAX30100.cpp -lshunyaInterfaces_core
# Run the compiled a.out
./a.out
```

