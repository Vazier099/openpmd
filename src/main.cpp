/**
 * @defgroup   HMI Nextion HMI
 *
 * @brief      This file implements showing values 
 *             and graph on the Nextion display.
 *
 * @date       2020-04-09
 */

/**
 * Two things: 
 * The component on the inactive page that you want to change must be vscope global.
 * When referencing the component you need to say which page it is on 
 * (the library is a bit weak in this regard), so either declare the component 
 * with the page prefix: NexText t0 = NexText(1, 1, "page1.t0"), or go lower 
 * level and write the command manually: sendCommand("page1.t0.txt="cheese").
 */

#include <stdio.h>
#include <stdbool.h>
#include <ctime>
#include <shunyaInterfaces.h>

#include "Nextion.h"
#include "MAX30100.h"
#include "pcf8591.h"
#include "ecg.h"
#include "log.h"

//TODO: Alarm.h, alarm GUI setting,

#define DEFAULT_VAL 00

#define LEVEL_HIGH      (30)
#define LEVEL_LOW       (0)

#define CH0_OFFSET  (40 - LEVEL_HIGH/2)
#define CH1_OFFSET  (CH0_OFFSET + 40 * 1)
#define CH2_OFFSET  (CH0_OFFSET + 40 * 2)

//Sensor AD8232
#define LO_NEG 31
#define LO_POS 29 
#define ECG_OUT 32

//Dedicated LED pins
#define LED_HEARTRATE 16
#define LED_SPO2 18
#define LED_ECG 22

#define BUZZERPIN 40
#define ALARM_SILENCE_PIN 36

#define DEFAULT_MAX_HR 80
#define DEFAULT_MIN_HR 60
#define DEFAULT_MIN_SPO2 90

#define ECG_BUF_SIZE 3
#define ECG_LEAD_CURR_VAL 0
#define ECG_LEAD_LAST_VAL 1
#define ECG_LEAD_2LAST_VAL 2

#define MIN_ECG_THRESHOLD 0.00035 //Considering the threshold to be 0.35mV(3.5 small squares in Y-direction). One small sq on ECG graph paper(Y-axis) is 0.1mV

static uint8_t ch0_data = LEVEL_LOW;
static uint8_t ch1_data = LEVEL_LOW;
static uint8_t ch2_data = LEVEL_LOW;

/**
 * Define Waveforms for the pages 0 and pages 1
 */
NexWaveform pg0S0 = NexWaveform(0, 5, "page0.s0");
NexWaveform pg1S0 = NexWaveform(1, 2, "page1.s0");

/* Callback functions for button click */
void alarmSilenceCallback(void *ptr);
void recordCallback(void *ptr);

MAX30100 *pulseOxymeter;

static int8_t isSoundAlarmOn = 1;

static int8_t isECGAlarmOn = 0;
static int8_t isHRAlarmOn = 0;
static int8_t isSpO2AlarmOn = 0;

struct critValues {
        /* BPM Values */
        int8_t bpm;
        /* SpO2 Values */
        float spo2;
        /*Ecg Values */
        int16_t leadI[ECG_BUF_SIZE];
        int16_t leadII[ECG_BUF_SIZE];
        int16_t leadIII[ECG_BUF_SIZE];
        int16_t leadaVL[ECG_BUF_SIZE];
        int16_t leadaVF[ECG_BUF_SIZE];
        int16_t leadaVR[ECG_BUF_SIZE];
        int16_t leadV[ECG_BUF_SIZE];
};

struct alarmThershold {
        int8_t heart_rate_max;
        int8_t heart_rate_min;
        float percent_spo2_min;
};

static struct critValues paitent;

struct alarmThershold thresh = {
        DEFAULT_MAX_HR,
        DEFAULT_MIN_HR,
        DEFAULT_MIN_SPO2
};

/*
 * Declare a number object [page id:0,component id:3, component name: "n0"]. 
 */
NexNumber p0n0 = NexNumber(0, 10, "page0.n0");
NexNumber p0n1 = NexNumber(0, 4, "page0.n1");
NexNumber p0n2 = NexNumber(0, 12, "page0.n2");
NexNumber p0n3 = NexNumber(0, 13, "page0.n3");

NexNumber p2n0 = NexNumber(2, 6, "page2.n0");
NexNumber p2n2 = NexNumber(2, 8, "page2.n2");
NexNumber p2n3 = NexNumber(2, 9, "page2.n3");

NexNumber p3n0 = NexNumber(3, 6, "page3.n0");
NexNumber p3n2 = NexNumber(3, 7, "page3.n2");
NexNumber p3n3 = NexNumber(3, 8, "page3.n3");


/**
 * Declare buttons here 
 */

NexButton p0b0 = NexButton(0, 1, "page0.b0");
NexButton p0b2 = NexButton(0, 9, "page0.b2");

NexButton p1b0 = NexButton(1, 1, "page1.b0");
NexButton p1b2 = NexButton(1, 5, "page1.b2");

NexButton p2b0 = NexButton(2, 1, "page2.b0");
NexButton p2b2 = NexButton(2, 5, "page2.b2");

NexButton p3b0 = NexButton(3, 1, "page3.b0");
NexButton p3b2 = NexButton(3, 5, "page3.b2");

/*
 * Register object n0, b0, b1, to the touch event list.  
 */
NexTouch *nex_listen_list[] = 
{
        &p0b0,
        &p0b2,
        &p1b0,
        &p1b2,
        &p2b0,
        &p2b2,
        &p3b0,
        &p3b2,
        NULL
};


/**
 * @brief Calls the alarm silence function when button is pressed.
 *
 */
void alarmSilenceCallback(void *ptr)
{
        /**
         * turn off the sound alarm
         */
        isSoundAlarmOn = 0;
}

/**
 * @brief Calls the record function when button is pressed.
 *
 */
void recordCallback(void *ptr)
{
        /**
         * Do nothing here
         */
}

/**
 * @brief      Gets the hour in localtime.
 *
 * @return     (int) the hour.
 */
int getHour () 
{
	time_t rawtime;
	time(&rawtime);
        struct tm *tm_struct = localtime(&rawtime);
        int hour = tm_struct->tm_hour;
        return hour;
}

/**
 * @brief      Gets the minute in localtime.
 *
 * @return     (int) The minute.
 */
int getMin () 
{
	time_t rawtime;
	time(&rawtime);
        struct tm *tm_struct = localtime(&rawtime);
        int min = tm_struct->tm_min;
        return min;
}

void check_ecg_alarm(int16_t *lead) 
{
        int16_t currECGval = lead[ECG_LEAD_CURR_VAL];
        int16_t lastECGval = lead[ECG_LEAD_LAST_VAL];
        int16_t secondLastECGval = lead[ECG_LEAD_2LAST_VAL];
        if ((lastECGval-currECGval) > 0 && (lastECGval-secondLastECGval) > 0 && currECGval > MIN_ECG_THRESHOLD) {
                isECGAlarmOn = 1;
        }
        /* Save the current values as last values */
        secondLastECGval = lastECGval;
        lastECGval = currECGval;
        
        lead[ECG_LEAD_CURR_VAL] = currECGval;
        lead[ECG_LEAD_LAST_VAL] = lastECGval;
        lead[ECG_LEAD_2LAST_VAL] = secondLastECGval;
}

void alarmLoop()
{	
        if (isECGAlarmOn){
                digitalWrite(LED_ECG, HIGH);
                if (isSoundAlarmOn) {
                        digitalWrite(BUZZERPIN, HIGH);                        
                }

        } else {
                digitalWrite(LED_ECG, LOW);
                digitalWrite(BUZZERPIN, LOW);
        }
        if (isSpO2AlarmOn){
                digitalWrite(LED_SPO2, HIGH);
                if (isSoundAlarmOn) {
                        digitalWrite(BUZZERPIN, HIGH);                        
                }

        } else {
                digitalWrite(LED_SPO2, LOW);
                digitalWrite(BUZZERPIN, LOW);
        }
        if (isHRAlarmOn){
                digitalWrite(LED_HEARTRATE, HIGH);
                if (isSoundAlarmOn) {
                        digitalWrite(BUZZERPIN, HIGH);                        
                }

        } else {
                digitalWrite(LED_HEARTRATE, LOW);
                digitalWrite(BUZZERPIN, LOW);
        }
}

void setup(void)
{
        /* Set the baud rate which is for debug and communicate with Nextion screen. */
        nexInit();

        //Configuring the pins as input and output
        pinMode(LED_HEARTRATE,OUTPUT);
        pinMode(LED_SPO2,OUTPUT);
        pinMode(LED_ECG,OUTPUT);
        pinMode(BUZZERPIN,OUTPUT);
        pinMode(ALARM_SILENCE_PIN,INPUT);

        //LED and Buzzer off

        digitalWrite(LED_SPO2, LOW);
        digitalWrite(LED_HEARTRATE, LOW);
        digitalWrite(LED_ECG, LOW);
        digitalWrite(BUZZERPIN, LOW);

        /* Attach the silence callback function to the Silence button */
        p0b0.attachPop(alarmSilenceCallback, &p0b0);
        p1b0.attachPop(alarmSilenceCallback, &p1b0);
        p2b0.attachPop(alarmSilenceCallback, &p2b0);
        p3b0.attachPop(alarmSilenceCallback, &p3b0);

        /* Attach the Record callback function to the Record button */
        p0b2.attachPop(alarmSilenceCallback, &p0b2);
        p1b2.attachPop(alarmSilenceCallback, &p1b2);
        p2b2.attachPop(alarmSilenceCallback, &p2b2);
        p3b2.attachPop(alarmSilenceCallback, &p3b2);
	
	ecgInit();

        /* Debug Print */
        writeToLog("setup done");
}

void loop(void)
{
        /* Get ECG Values */
        int val = 0;
        val = getLeadI();
        if (val < 0) {
                writeToLog("ECG Lead I value negative");
                paitent.leadI[ECG_LEAD_CURR_VAL] = 0;
        } else {
                paitent.leadI[ECG_LEAD_CURR_VAL] = val;
        }
        val = 0;
        val = getLeadII();;
        if (val < 0) {
                writeToLog("ECG Lead II value negative");
                paitent.leadII[ECG_LEAD_CURR_VAL] = 0;
        } else {
                paitent.leadII[ECG_LEAD_CURR_VAL] = val;
        }
        val = 0;
        val = getLeadIII();
        if (val < 0) {
                writeToLog("ECG Lead III value negative");
                paitent.leadIII[ECG_LEAD_CURR_VAL] = 0;
        } else {
                paitent.leadIII[ECG_LEAD_CURR_VAL] = val;
        }
        val = 0;
        val = getLeadaVF();
        if (val < 0) {
                writeToLog("ECG Lead aVF value negative");
                paitent.leadaVF[ECG_LEAD_CURR_VAL] = 0;
        } else {
                paitent.leadaVF[ECG_LEAD_CURR_VAL] = val;
        }
                val = 0;
        val = getLeadaVL();
        if (val < 0) {
                writeToLog("ECG Lead aVL value negative");
                paitent.leadaVL[ECG_LEAD_CURR_VAL] = 0;
        } else {
                paitent.leadaVL[ECG_LEAD_CURR_VAL] = val;
        }
        val = 0;
        val = getLeadaVR();
        if (val < 0) {
                writeToLog("ECG Lead aVR value negative");
                paitent.leadaVR[ECG_LEAD_CURR_VAL] = 0;
        } else {
                paitent.leadaVR[ECG_LEAD_CURR_VAL] = val;
        }
                val = 0;
        val = getChestLeads();
        if (val < 0) {
                writeToLog("ECG Lead V value negative");
                paitent.leadV[ECG_LEAD_CURR_VAL] = 0;
        } else {
                paitent.leadV[ECG_LEAD_CURR_VAL] = val;
        }

        /* Get Heart Rate and SpO2 value */
        pulseoxymeter_t result = pulseOxymeter-> update();

        if (result.pulseDetected == true){
                paitent.bpm = result.heartBPM;
                paitent.spo2 = result.SaO2;
        }

        /* Update Waveforms on Page 1 */           
        pg0S0.addValue(0, CH0_OFFSET + (paitent.leadaVR[ECG_LEAD_CURR_VAL]/10));

        /* Update Waveforms on Page 2*/
        pg1S0.addValue(0, CH0_OFFSET + (paitent.leadaVR[ECG_LEAD_CURR_VAL]/10));
        pg1S0.addValue(1, CH1_OFFSET + (paitent.leadI[ECG_LEAD_CURR_VAL]/10));
        pg1S0.addValue(2, CH2_OFFSET + (paitent.leadII[ECG_LEAD_CURR_VAL]/10));

        /**
         * Set the Hour and minute value on the page.
         */
        int hour = getHour();
        int min = getMin();
        p0n2.setValue(hour);
        p0n3.setValue(min);
        //p1n2.setValue(hour);
        //p1n3.setValue(min);
        p2n2.setValue(hour);
        p2n3.setValue(min);
        p3n2.setValue(hour);
        p3n3.setValue(min);

        /**
         * Update the Heart Rate throughout all the pages.
         */
        p0n0.setValue((int)paitent.bpm);
        p2n0.setValue((int)paitent.bpm);
        /**
         * Update the SpO2 Value throughout all the pages.
         */
        p0n1.setValue((int)paitent.spo2);
        p3n0.setValue((int)paitent.spo2);

        /* ------------------------------------------------------ */
        /*              Conditions ECG */
        /* ------------------------------------------------------ */
             
        check_ecg_alarm(paitent.leadI);
        check_ecg_alarm(paitent.leadII);
        check_ecg_alarm(paitent.leadIII);
        check_ecg_alarm(paitent.leadaVF);
        check_ecg_alarm(paitent.leadaVL);
        check_ecg_alarm(paitent.leadaVR);
        check_ecg_alarm(paitent.leadV);

        /* ------------------------------------------------------ */
        /*              Conditions Heart Rate */
        /* ------------------------------------------------------ */

        if(paitent.bpm > thresh.heart_rate_max || paitent.bpm < thresh.heart_rate_min) {
                isHRAlarmOn = 1;
        }

        /* ------------------------------------------------------ */
        /*              Conditions SPO2 */
        /* ------------------------------------------------------ */
        
        if(paitent.spo2 < thresh.percent_spo2_min) {
                isSpO2AlarmOn = 1;
        }

        /* ------------------------------------------------------ */
        /*              Conditions Alarms stop button */
        /* ------------------------------------------------------ */
        if (digitalRead(ALARM_SILENCE_PIN) == HIGH) {
                isSoundAlarmOn = 0;
        }    

        alarmLoop();
        /*
         * When a pop or push event occurred every time, 
         * the corresponding component[right page id and component id] in touch 
         * event list will be asked.
         */
        nexLoop(nex_listen_list);   
}

//Main Function
int main()
{
        setup();

        while(1) {
                loop();
        }

	return 0;
}
