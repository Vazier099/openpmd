/**
 * @defgroup   HMI Nextion HMI
 *
 * @brief      This file implements showing values 
 *             and graph on the Nextion display.
 *
 * @date       2020-04-09
 */

/**
 * Two things: 
 * The component on the inactive page that you want to change must be vscope global.
 * When referencing the component you need to say which page it is on 
 * (the library is a bit weak in this regard), so either declare the component 
 * with the page prefix: NexText t0 = NexText(1, 1, "page1.t0"), or go lower 
 * level and write the command manually: sendCommand("page1.t0.txt="cheese").
 */

#include <stdio.h>
#include <shunyaInterfaces.h>
#include "Nextion.h"

#define DEFAULT_VAL 00

#define LEVEL_HIGH      (30)
#define LEVEL_LOW       (0)

#define CH0_OFFSET  (40 - LEVEL_HIGH/2)
#define CH1_OFFSET  (CH0_OFFSET + 40 * 1)
#define CH2_OFFSET  (CH0_OFFSET + 40 * 2)

static uint8_t ch0_data = LEVEL_LOW;
static uint8_t ch1_data = LEVEL_LOW;
static uint8_t ch2_data = LEVEL_LOW;

/**
 * Define Waveforms for the pages 0 and pages 1
 */
NexWaveform pg0S0 = NexWaveform(0, 5, "page0.s0");
NexWaveform pg1S0 = NexWaveform(1, 2, "page1.s0");

/* Callback functions for button click */
void alarmSilenceCallback(void *ptr);
void recordCallback(void *ptr);

/*
 * Declare a number object [page id:0,component id:3, component name: "n0"]. 
 */
NexNumber p0n0 = NexNumber(0, 10, "page0.n0");
NexNumber p0n1 = NexNumber(0, 4, "page0.n1");
NexNumber p0n2 = NexNumber(0, 12, "page0.n2");
NexNumber p0n3 = NexNumber(0, 13, "page0.n3");

NexNumber p2n0 = NexNumber(2, 6, "page2.n0");
NexNumber p2n2 = NexNumber(2, 8, "page2.n2");
NexNumber p2n3 = NexNumber(2, 9, "page2.n3");

NexNumber p3n0 = NexNumber(3, 6, "page3.n0");
NexNumber p3n2 = NexNumber(3, 7, "page3.n2");
NexNumber p3n3 = NexNumber(3, 8, "page3.n3");


/**
 * Declare buttons here 
 */

NexButton p0b0 = NexButton(0, 1, "page0.b0");
NexButton p0b2 = NexButton(0, 9, "page0.b2");

NexButton p1b0 = NexButton(1, 1, "page1.b0");
NexButton p1b2 = NexButton(1, 5, "page1.b2");

NexButton p2b0 = NexButton(2, 1, "page2.b0");
NexButton p2b2 = NexButton(2, 5, "page2.b2");

NexButton p3b0 = NexButton(3, 1, "page3.b0");
NexButton p3b2 = NexButton(3, 5, "page3.b2");

/*
 * Register object n0, b0, b1, to the touch event list.  
 */
NexTouch *nex_listen_list[] = 
{
	&p0b0,
	&p0b2,
	&p1b0,
	&p1b2,
	&p2b0,
	&p2b2,
	&p3b0,
	&p3b2,
	NULL
};


/**
 * @brief Calls the alarm silence function when button is pressed.
 *
 */
void alarmSilenceCallback(void *ptr)
{
	/**
	 * Call the alarm silence here
	 */
}

/**
 * @brief Calls the record function when button is pressed.
 *
 */
void recordCallback(void *ptr)
{
	/**
	 * Call the record here
	 */
}

/**
 * @brief      Gets the hour in localtime.
 *
 * @return     (int) the hour.
 */
int getHour () 
{
	struct tm *tm_struct = localtime(time(NULL));
	int hour = tm_struct->tm_hour;
	return hour;
}

/**
 * @brief      Gets the minute in localtime.
 *
 * @return     (int) The minute.
 */
int getMin () 
{
	struct tm *tm_struct = localtime(time(NULL));
	int min = tm_struct->tm_min;
	return min;
}

void setup(void)
{
	/* Set the baud rate which is for debug and communicate with Nextion screen. */
	nexInit();

	/* Attach the silence callback function to the Silence button */
	p0b0.attachPop(alarmSilenceCallback, &p0b0);
	p1b0.attachPop(alarmSilenceCallback, &p1b0);
	p2b0.attachPop(alarmSilenceCallback, &p2b0);
	p3b0.attachPop(alarmSilenceCallback, &p3b0);

	/* Attach the Record callback function to the Record button */
	p0b2.attachPop(alarmSilenceCallback, &p0b2);
	p1b2.attachPop(alarmSilenceCallback, &p1b2);
	p2b2.attachPop(alarmSilenceCallback, &p2b2);
	p3b2.attachPop(alarmSilenceCallback, &p3b2);

	/* Debug Print */
	printf("setup done");
}

void loop(void)
{
	/**
	 * Update all the waveforms
	 */
	static uint32_t started = 0;
	if (millis() - started >= 2000) {
		started = millis();    
		if (LEVEL_HIGH == ch0_data) {
			ch0_data = LEVEL_LOW;
		}
		else {
			ch0_data = LEVEL_HIGH;
		}
	}

	pg1S0.addValue(0, CH0_OFFSET + ch0_data);

	/**
	 * Page 2 waveform currently a dummy value
	 * will update the ECG value 
	 */
	ch1_data = ch0_data + 2;
	ch2_data = ch0_data + 5;

	pg2S0.addValue(0, CH0_OFFSET + ch0_data);
	pg2S0.addValue(1, CH1_OFFSET + ch1_data);
	pg2S0.addValue(2, CH2_OFFSET + ch2_data);

	/**
	 * Set the Hour and minute value on the page.
	 */
	int hour = getHour();
	int min = getMin();
	p0n2.setValue(hour);
	p0n3.setValue(min);
	p1n2.setValue(hour);
	p1n3.setValue(min);
	p2n2.setValue(hour);
	p2n3.setValue(min);
	p3n2.setValue(hour);
	p3n3.setValue(min);

	/**
	 * Update the Heart Rate throughout all the pages.
	 */
	p0n0.setValue(62);
	p2n0.setValue(62);
	/**
	 * Update the SpO2 Value throughout all the pages.
	 */
	p0n1.setValue(97);
	p3n0.setValue(97);
	/*
	 * When a pop or push event occurred every time, 
	 * the corresponding component[right page id and component id] in touch 
	 * event list will be asked.
	 */
	nexLoop(nex_listen_list);   
}

