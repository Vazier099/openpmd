cmake_minimum_required(VERSION 3.2)

project(openpmd VERSION 0.0.1 LANGUAGES C CXX)

# Must use GNUInstallDirs to install libraries into correct
# locations on all platforms.
include(GNUInstallDirs)

add_executable(openpmd
	src/ecg/ecg.c
	src/logging/log.c
	src/MAX30100/MAX30100.cpp
	src/main.cpp)

# Define headers for this library. PUBLIC headers are used for
# compiling the library, and will be added to consumers' build
# paths.
target_include_directories(openpmd PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
    $<INSTALL_INTERFACE:include>
    PRIVATE src include)

target_link_libraries(openpmd 
    -lsiCloud
	-lsiDash
	-lsiPeripheral
	-lsiYaml
	-lsiCore
	-lcurl)

add_executable(openpmd_reset
	"src/reset/reset.c" )

target_include_directories(openpmd_reset PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
    $<INSTALL_INTERFACE:include>
    PRIVATE src include)

target_link_libraries(openpmd_reset
	-lsiDash
	-lsiPeripheral
	-lsiYaml
	-lsiCore
	-lcurl)



# 'make install' to the correct locations (provided by GNUInstallDirs).
install(TARGETS ${PROJECT_NAME} EXPORT ${PROJECT_NAME}Config
    ARCHIVE  DESTINATION ${CMAKE_INSTALL_LIBDIR}
    LIBRARY  DESTINATION ${CMAKE_INSTALL_LIBDIR}
    RUNTIME  DESTINATION ${CMAKE_INSTALL_BINDIR})  # This is for Windows

install(DIRECTORY include/ DESTINATION ${CMAKE_INSTALL_INCLUDEDIR})


