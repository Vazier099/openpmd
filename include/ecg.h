#ifndef ECG_H
#define ECG_H


#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

//TODO: Fix advance settings of the PCF8581 sensor

#define adc1_Channel0 0
#define adc1_Channel1 1
#define adc1_Channel2 2
#define adc1_Channel3 3

#define adc2_Channel0 4
#define adc2_Channel1 5
#define adc2_Channel2 6
#define adc2_Channel3 7

//Sensor 1 : Lead I
#define LO_POS1 7
#define LO_NEG1 8

//Sensor 2 : Lead II
#define LO_POS2 10 
#define LO_NEG2 12
 
//Sensor 3 : Lead III
#define LO_POS3 11
#define LO_NEG3 13

//Sensor 4 : Lead aVR
#define LO_POS4 15
#define LO_NEG4 16

//Sensor 5 : Lead aVL
#define LO_POS5 18
#define LO_NEG5 19

//Sensor 6 : Lead aVF 
#define LO_POS6 21
#define LO_NEG6 23

//Sensor 7 : Cheast lead V1 to V6
#define LO_POS7 24
#define LO_NEG7 26

//Function for Lead I
extern int getLeadI();

//Function for Lead II
extern int getLeadII();

//Function for Lead III
extern int getLeadIII();

//Function for Lead aVR
extern int getLeadaVR();

//Function for Lead aVL
extern int getLeadaVL();

//Function for aVF
extern int getLeadaVF();

//Function for chest lead
extern int getChestLeads();

//Function to configure pins as input 
extern void ecgInit();

#ifdef __cplusplus
}
#endif

#endif
