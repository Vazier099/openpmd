
#ifndef ALARMS_H
#define ALARMS_H

//Dedicated LED pins
#define LED_HEARTRATE 15
#define LED_SPO2 13
#define LED_ECG 11


#define buzzerPin 16 
#define alarmStopButtonPin 12

#define DEFAULT_MAX_HR 80
#define DEFAULT_MIN_HR 60
#define DEFAULT_MIN_SPO2 90

#define MIN_ECG_THRESHOLD 0.00035 //Considering the threshold to be 0.35mV(3.5 small squares in Y-direction). One small sq on ECG graph paper(Y-axis) is 0.1mV


extern void alarms_init();
extern void alarms_ring (void);

#endif
