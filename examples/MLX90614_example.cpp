/*************************************************** 
  This is a library example for the MLX90614 Temp Sensor
  Designed specifically to work with the MLX90614 sensors in the
  adafruit shop
  ----> https://www.adafruit.com/products/1747 3V version
  ----> https://www.adafruit.com/products/1748 5V version
  These sensors use I2C to communicate, 2 pins are required to  
  interface
  Adafruit invests time and resources providing this open source code, 
  please support Adafruit and open-source hardware by purchasing 
  products from Adafruit!
  Written by Limor Fried/Ladyada for Adafruit Industries.  
  BSD license, all text above must be included in any redistribution
 ****************************************************/

#include <iostream>
#include <shunyaInterfaces.h>
#include "Adafruit_MLX90614.h"
#include <Wire.h>

using namespace std; 

Adafruit_MLX90614 mlx = Adafruit_MLX90614();

int main()
{
  libInit();

  cout <<"Adafruit MLX90614 test" << endl;  

  mlx.begin();  


while(1){
  cout << "Ambient = " ; cout << (mlx.readAmbientTempC()); 
  cout << "*C\tObject = "; cout << (mlx.readObjectTempC()); cout << "*C\n";
  cout << "Ambient = " ; cout << (mlx.readAmbientTempF()); 
  cout << "*F\tObject = "; cout << mlx.readObjectTempF(); cout << "*F\n";

  cout << "\n" ;
  delay(500);
}
return 0;
}
