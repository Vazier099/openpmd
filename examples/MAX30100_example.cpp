/**
****************************************************************************
* @file         MAX30100_example.c
* @author       Vaibhav Shinde (@VaibKS) and Shunya Core Team
* @brief        This file provides the source code for interfacing 
                the MAX30100 heart rate sensor.
*         
@verbatim
-----------------------------------------------------------------------------
                        ## Description ## 
-----------------------------------------------------------------------------
## Introduction
This file contains an example code for interfacing the MAX30100 heart rate sensor
using Shunya Interfaces library.

## Connections
There are 3 pins to Water Flow Sensor module YF-S102
1. Vin (also called +) - Connect it to 3.3V on the dev Board as the sensor works between 1.8V to 3.3V.
2. GND (also called -) - Connect it to GND on the dev Board
3. Connect the SDL and SCL pins to the SDL and SCL pins on the board which are both for i2c transmission.


## Compile 
1. Open terminal 
2. Run command `g++ -o heart-rate MAX30100_example.cpp -lshunyaInterfaces_core`

## Run 
1. Open terminal 
2. Run command `./heart-rate`

@endverbatim
******************************************************************************
* @attention
*
* <h2><center>&copy; Copyright (c) 2019 IoTIoT.in
* All rights reserved.</center></h2>
*
* This software component is licensed by IoTIoT under GPLv3 license,
* the "License"; You may not use this file except in compliance with the
* License. You may obtain a copy of the License at:
*                        opensource.org/licenses/GPL-3.0
*
******************************************************************************
*/

#include <iostream>
#include <math.h>
#include <shunyaInterfaces.h>
#include "MAX30100.h"

// #include <Wire.h>

using namespace std;

MAX30100 *pulseOxymeter;

int main()
{
    libInit();

    cout << "Pulse oxymeter test!" << endl;

    // pulseOxymeter = new MAX30100( DEFAULT_OPERATING_MODE, DEFAULT_SAMPLING_RATE, DEFAULT_LED_PULSE_WIDTH, DEFAULT_IR_LED_CURRENT, true, true );
    pulseOxymeter = new MAX30100();
    pinMode(2, OUTPUT);

    while (1)
    {
        //You have to call update with frequency at least 37Hz. But the closer you call it to 100Hz the better, the filter will work.
        pulseoxymeter_t result = pulseOxymeter->update();

        if (result.pulseDetected == true)
        {
            cout << "BEAT" << endl;

            cout << "BPM: " << endl;
            cout << (result.heartBPM) << endl;
            cout << " | " << endl;

            cout << "SaO2: " << endl;
            cout << (result.SaO2) << endl;
            cout << "%" << endl;

            cout << "{P2|BPM|255,40,0|" << endl;
            cout << (result.heartBPM) << endl;
            cout << "|SaO2|0,0,255|" << endl;
            cout << (result.SaO2) << endl;
            cout << "}" << endl;
        }

        //These are special packets for FlexiPlot plotting tool
        // cout << "{P0|IR|0,0,255|" << endl;
        // cout << (result.dcFilteredIR) << endl;
        // cout << "|RED|255,0,0|" << endl;
        // cout << (result.dcFilteredRed) << endl;
        // cout << "}" << endl;

        // cout << "{P1|RED|255,0,255|" << endl;
        // cout << (result.irCardiogram) << endl;
        // cout << "|BEAT|0,0,255|" << endl;
        // cout << (result.lastBeatThreshold) << endl;
        // cout << "}" << endl;

        delay(10);

        //Basic way of determening execution of the loop via oscoliscope
        digitalWrite(2, !digitalRead(2));
    }
    return 0;
}
